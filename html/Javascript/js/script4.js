var element = document.getElementById('num');

console.log(element);
console.log(element.nodeName);
console.log(element.textContent);
console.log(element.innerHTML);
console.log(element.outerHTML);

var div = document.createElement('div');
div.textContent = 'Esto es un div insertado con Js';

var app = document.querySelector('#app');
app.appendChild(div);

console.log(5-'7');
console.log('7'-'5');
console.log(5+'5.1a');
console.log(5-'x');