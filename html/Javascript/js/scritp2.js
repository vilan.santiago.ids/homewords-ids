var a = 5;

console.log(a);

function x(){
    console.log(a);
    var a =5;

    console.log(a);
    console.log(window.a);
}

var p;
console.log('Antes:', p);
for(p =0; p<3; p++)
    console.log('--',p);

console.log('Despues:',p);

x();
console.log(a);

//Los objetos si se pueden cambiar sus valores
const user = {name: 'Juan'};

user.name = 'Luis';

console.log(user.name);