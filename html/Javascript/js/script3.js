//Objetos

var objeto = new Object();

var objeto2 = {};

var player = {
    name: 'texto',
    life: 99,
    strength: 10,
};

var texto1 = '¡Hola a todos!';
var texto2 = 'Otro mensaje de texto';

var texto11 = new String('¡Hola a todos!');
var texto22 = new String('Otro mensaje de texto');

var array = new Array('a', 'b', 'c');
var array1 = ['a', 'b', 'c'];
var empty = [];
var mixto = ['a', 5, true];
var array2 = ['a', 'b', 'c', 'd', 'e'];

var a = [1,8,2,32,9,7,4];

console.log(array);

console.log('Manera 1');
console.log("name: ",player.name);
console.log("life: ",player.life);
console.log('Manera 2');
console.log("name:",player['name']);
console.log("life:",player['life']);

console.log(texto1.toString());
console.log(texto2.toString());
console.log(texto11.toString());
console.log(texto22.toString());

console.log(texto1.length);
console.log(texto2.length);
console.log(texto11.length);
console.log(texto22.length);

console.log('Arreglos');
console.log(array);
console.log(array1);
console.log(empty);
console.log(mixto);
console.log(array[5]);

console.log("Arreglo original",array);
array.push('d');
console.log("push",array);
array.pop();
console.log("pop",array);
array.unshift('Z')
console.log("unshift",array);
array.shift();
console.log("shift",array);
console.log("Arreglo 2",array1);
array = array.concat(array1);
console.log("Arreglo nuevo",array);
console.log("Arreglo",array2);
;
console.log(array2.slice(2,4));
console.log(array2.slice(2,2));
console.log(array2.slice(1,0,'z','x'));

console.log('Antes',a);
a.sort();
console.log('Ordenado',a);

var fc = function(a,b){
    return a-b;
}
console.log('arreglo antes de fc',a)
a.sort(fc);
console.log('Función fc',a);

var arr = [2,3,9,54,6,1];

var f=function(a,b){
    if(a>b){
        return 1;
    }else if(a<b){
        return -1;
    }
}
console.log('arreglo antes de f',arr)
console.log('funcion f',arr.sort(f));