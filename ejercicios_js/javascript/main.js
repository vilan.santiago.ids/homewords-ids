
var personArr = [
        {
            "personId": 123,
            "name" : "Jhon",
            "city" : "Melbourne",
            "phoneNo": "1234567890"
        },
        {
            "personId": 124,
            "name" : "Amelia",
            "city" : "Sydney",
            "phoneNo": "1234567890"
        },
        {
            "personId": 125,
            "name" : "Emily",
            "city" : "Perth",
            "phoneNo": "1234567890"
        },
        {
            "personId": 126,
            "name" : "Abraham",
            "city" : "Perth",
            "phoneNo": "1234567890"
        }    
];

function createTable()
{
    var table = document.getElementById("t1");
    var id, name, city, tel;

    for( let i=0; i<personArr.length; i++ )
    {
        var createTR = document.createElement('tr');
        table.appendChild(createTR);

        var tdId = document.createElement('td');
        var tdName = document.createElement('td');
        var tdCity = document.createElement('td');
        var tdTel = document.createElement('td');

        tdId.appendChild(document.createTextNode(personArr[i].personId));
        tdName.appendChild(document.createTextNode(personArr[i].name));
        tdCity.appendChild(document.createTextNode(personArr[i].city));
        tdTel.appendChild(document.createTextNode(personArr[i].phoneNo));

        createTR.appendChild(tdId);
        createTR.appendChild(tdName);
        createTR.appendChild(tdCity);
        createTR.appendChild(tdTel);
        //console.log(`${id} ${name} ${city} ${tel}`);
        
    }
}

function alerta(clicked_id)
{
//    alert(clicked_id);
    var id;
    var iso;
    var dc;
    /*var elemento = document.getElementById(clicked_id);

    
    

    alert('Elemento Seleccionado:'+`\n"ID Elemento: {${id}"}\n "ISO ID: {${iso}}"\n "Dial Code: {${dc}}"\n`); */
    var elemento = document.getElementById(clicked_id);
    id = elemento.getAttribute('id');
    iso = elemento.getAttribute('data-id');
    dc = elemento.getAttribute('data-dial-code');

    
    elemento.addEventListener('click', () => {
        alert('Elemento Seleccionado:'+`\n"ID Elemento: {${id}"}\n "ISO ID: {${iso}}"\n "Dial Code: {${dc}}"\n`); 
    })
}



createTable();