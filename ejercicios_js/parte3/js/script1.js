var anObject = {
    foo: 'bar',
    length: 'interesting',
    '0':'zero!',
    '1':'one!'
};

var anArray = ['zero.', 'one.'];

console.log(anArray[0], anObject[0]);
// imprimira [ zero. y zero! ] porque estamos accediendo
// por medio de la notación de corches y es lo mismo para el coso
// de anObject busca el atributo "0"

console.log(anArray[1], anObject[1]);
// imprimira [one. y one! ] porque estamos accediendo
// por medio de la notación de corches y es lo mismo para el coso
// de anObject busca el atributo "1"

console.log(anArray.length, anObject.length);
// imprime 2 y la cadena interesting. en el primer caso
// llamamos el metodo length que retorna el número de elementos del arreglo
// para el caso 2 estamos accediendo al atributo length del objeto anObject
// y nos regresarar su valor

console.log(anArray.foo, anObject.foo);
// imprimira undefined y bar, para el primer caso solo se puede acceder a los
// elementos del arreglo por medio de la notacion de "" y por esta razon nos retorna
// undefined para el segundo caso el objeto anObject cuenta con un atributo llamado 
// foo y nos retornara su valor en ests caso bar

console.log( typeof anArray == 'object', typeof anObject == 'object' );
/* retornara true y true. Para el primer caso la función typeof retorna el tipo
de variable u objeto, anArray es un objeto que permite almacenar varios valores (array) y al comparar
el resultado de typeof con 'object' no retorna un true. para el segundo caso es practicamente lo mismo
porque anObject es un objeto por la manera en que lo declaramos,
ahora al utilizar typeof y utilizar el operador de estrictamente igual nos retornara un true.
*/

console.log(anArray instanceof Object, anObject instanceof Object);
/** Imprimira true y true. Porque instanceof es un operador que comprueba el tipo de objeto 
 * ( valga la redundacia) de un objeto. en el primer caso anArray es un
 * y como anArray es un objeto arreglo pero un objeto a final de cuentas 
 * retorna true y para el caso de anObject el operador instanceof retornara
 * true porque es un objeto de tipo objeto valga la redundancia.
 *  */


 console.log(anArray instanceof Array, anObject instanceof Array);
 /** retornara true y false, para el primer caso nos retornara true porque
  * anArray es un objeto Array, y para siguiente caso anObjecto es un objeto pero
  * no del tipo Array por esto retornara false.
  * 
  * 
  */

 console.log(Array.isArray(anArray), Array.isArray(anObject));
 /** imprimira true y false. Para el caso de la primera llamada de la
  * función isArray retorna true porque anArray es un objeto de tipo arreglo.
  * Y la función isArray retorna true si el parametro que le enviamos es un objeto de tipo arreglo,
  * en caso contrario retorna false como es para el segundo caso al pasar como
  * parametro anObject que es un Objeto pero no un objeto Array
  * 
  */

 // Ejerecicio 2
 var obj = {
     a: "hello",
     b: "this is",
     c: "javascript",
     arreglo : function(){
         return [this.a, this.b, this.c];
     }
 };

 var array = obj.arreglo();
 console.log(array);

 // Ejercicio 3
 
 function pares()
 {
     for( let i=0; i <=98; i+=2)
     {
         console.log(i);
     }
 }

 //pares();

 // Ejercicio 4
 let zero = 0;
 function multiply(x) { return x*2; }
 function add( a= 1 + zero, b = a, c= b+a, d =multiply(c)){
     console.log((a+b+c),d);
 }
 
 add(1);
 /**
  * a = 1 +0 => 1
  * b = a => 1
  * c = b + a => 2
  * d = c * 2 => 4
  * a+b+c = 4
  * d = 4 
  */
 add(3);
 /**
  * a = 3 => 3
  * b = a => 3
  * c = b + a => 6
  * d = c * 2 => 12
  * a+b+c = 12
  * d = 12 
  */
 add(2,7);
 /**
  * a = 2 => 2
  * b = 7 => 7
  * c = b + a => 9
  * d = c * 2 => 18
  * a+b+c = 18
  * d = 18 
  */
 add(1,2,5);
 /**
  * a = 1  => 1
  * b = 2 => 2
  * c = 5 => 5
  * d = c * 2 => 10
  * a+b+c = 5
  * d = 10 
  */
 add(1,2,5,10);
 /**
  * a = 1 => 1
  * b = 2 => 2
  * c = 5 => 5
  * d = 10 => 10
  * a+b+c = 8
  * d = 10
  */

 class MyClass{
     constructor()
     {
         this.name_ = [];
     }

     set name(value)
     {
         this.name_.push(value);
     }

     get name(){
         return this.name_[this.name_.length -1];
     }
 }

 const myClassInstance = new MyClass();
 myClassInstance.name = 'Joe';
 myClassInstance.name = 'Bob';

 console.log(myClassInstance.name);
/**
 * aqui llamamos implicitaente el metodo get name()
 * y este método nos retorna el ultimo elemento agregado
 * dentro del arreglo name_ 
 */

 console.log(myClassInstance.name_);

/**
 * Imprimos el arreglo que tiene el objeto myClassInstance.
 * Este artributo tiene dos elementos y son 'Joe' y 'Bob'
 */

const classInstance = new class{
    get prop(){
        return 5;
    }
};

classInstance.prop = 10;

console.log(classInstance.prop);
/**
 * Retorna 5 porque cuando ejecutamos la sentencia 
 * classInstance.prop implicitamente llamamos el metodo
 * get prop y este método retorna siempre 5, aun cuando 
 * le agregemos un valor a nuestro atributo prop
 */

class Queue{
    constructor()
    {
        const list = [];

        this.euqueue = function(type)
        {
          list.push(type);
          return type;
        };

        this.dequeue = function()
        {
            return list.shift();
        };
    }
}


var q = new Queue;

q.euqueue(9);
q.euqueue(8);
q.euqueue(7);

console.log("clase Queue");

console.log(q.dequeue());
/**
 * llamamos el método dequeue del objeto q
 * no retornara 9 porque la clase Queue
 * funciona como si fuera una cola
 * y en este tipo de estructura se caracteriza
 * en sacar el primer elemento que entro en ella.
 * Al llamar el metodo dequeue nos retorna el que 
 * fue su primer elemento 9.
 * 
 */
console.log(q.dequeue());
/**
 * Aqui imprimimos un 8, porque fue el segundo elemento
 * que se ingreso en el arreglo del objeto q,
 * cuando ejecutamos la función anteriormente dequeue,
 * el elemento 8 paso al inicio del arreglo y al
 * llama el método dequeue retiramos el primer elemento
 * y lo pintamos en la terminal.
 */
console.log(q.dequeue());
/**
 * Imprimimos 7, ya que fue el ultimo elemento del arreglo
 * del objeto q y al ejecutar el método dequeue quitamos este
 * ultimo elmento que estaba al inicio del arreglo y lo imprimimos
 * en la terminal
 */
console.log(q);
/** En la terminal nos muestra el nombre de la clase a la que pertence
 * y los métodos que posee dequeue y euqueue
 */


console.log(Object.keys(q));
/** Imprime un array con los metodos que posee el objeto q */

