// Página 2
function one()
{
    var a =0;
    var str = 'not a';
    var b = '';
    b = a === 0 ? ( a = 1, str += ' test' ) : (a =2);
    console.log("a = ", a, ", b = ", b);
}

function two()
{
    var a =1;
    
    a === 1 ? alert( 'Hey, it is 1!' ) : 0;
    console.log("a = ", a);
}

function three()
{
    var a;

    a === 1 ? alert('Hey, it is 1!') : alert('Weird, what could it be?');
    if (a === 1)
        alert('Hey, it is 1!')
    else
        alert('Weird, what could it be?');
    
}

//fin página 2

// Page 3
function four(){
    var animal = 'kitty';

        for( var i =0; i < 5; ++i )
        {    
            // la sentencia break no se puede ejecutar porque 
            // no retorna un valor el operador ? : (ternario)
            // solo puede tomar valores 
            //(animal === 'kitty' )  ? break; console.log(i);
            // esta funcionaria
            if( (animal === 'kitty' ) )
            {   console.log("Entro al if con break"); break;}
            else
            console.log(i)
            
            
        }
}

function five( )
{
    var value = 1;

        switch( value )
        {
            case 1:
                // imprimer "Siempre correre"
                console.log("I will always run");
            break;
            case 2:
                // Imprime "Unca correre"
                console.log("I will never run");
            break;
        }
}

// End Page 3

// to of page 4
function six()
{
    var animal = 'Lion';

        switch(animal)
        {
            case 'Dog':
                console.log('I will not run since animal !== "Dog"');
            break;

            case 'Cat':
                console.log('I will not run since animal !== "Cat"');
            break;

            default:
                console.log('I will run since animal does no match any other case');
            
        }
}

function john()
{
    return 'John';
}

function jacob()
{
    return 'Jacob';
}

function seven()
{
    var name = john();
    //var name = 'Jane';
    //var name = 'John Jacob Jingleheimer Schmidt';

        switch( name )
        {
            case john():
                console.log('I will run it name === "John"');
            break;

            case 'Ja' + 'ne':
                console.log('I will run it name === "Jane"');
            break;

            case john() + ' ' + jacob() + ' Jingleheimer Schmidt':
                console.log('His name is equal to name too!');
            break;
        }
}

// end page 4

// to of page 5
function eight()
{
    var x = "c";

        switch(x)
        {
            case "a":
            case "b":
            case "c":
                console.log("Either a, b, or c was selected.");
            break;

            case "d":
                console.log("Only d was selected.");
            break;

            default:
                console.log("No case was matched.");
            break;  // precautionary break if case order changes
        }
}

function nine()
{
    var x = 5 + 7;
    console.log("x es igual a ", x);
    x = 5 + "7";
    console.log("x es igual a ", x);
    x = "5" + 7;
    console.log("x es igual a ", x);
    x = 5 - 7;
    console.log("x es igual a ", x);
    x = "5" - 7;
    console.log("x es igual a ", x);
    x = 5 - "x";
    console.log("x es igual a ", x);
}
// end page 5

// to of page 6
function ten()
{
    var a = 'hello' || '';
    var b = '' || [];
    var c = '' || undefined;
    var d = 1 || 5;
    var e = 0 || {};
    var f = 0 || '' || 5;
    var g = '' || 'yay' || 'boo';

    console.log("a es igual a: ", a);
    console.log("b es igual a: ", b);
    console.log("c es igual a: ", c);
    console.log("d es igual a: ", d);
    console.log("e es igual a: ", e);
    console.log("f es igual a: ", f);
    console.log("g es igual a: ", g);
}

function eleven()
{
    var a = 'hello' && '';
    var b = '' && [];
    var c = undefined && 0;
    var d = 1 && 5;
    var e = 0 && {};
    var f = 'hi' && [] && 'done';
    var g = 'bye' && undefined && 'adios';

    console.log("a es igual a: ", a);
    console.log("b es igual a: ", b);
    console.log("c es igual a: ", c);
    console.log("d es igual a: ", d);
    console.log("e es igual a: ", e);
    console.log("f es igual a: ", f);
    console.log("g es igual a: ", g);

}

// end page 6

// to of page 7

var foo = function(val){
    return val || 'default';
}

function twelty()
{
    console.log( foo('burger') );
    console.log( foo(100) );
    console.log( foo([]) );
    console.log( foo(0) );
    console.log( foo(undefined) );
}

function thirteen()
{
    var age = 17, height= 74;

    var isLegal = age >= 18;
    var tall = height >= 5.11;
    var suitable = isLegal && tall;
    var isRoyalty = status === 'royalty';
    var specialCase = isRoyalty && hasInvitation;
    var canEnterOurBar = suiteble || specialCase;

    console.log(`isLegal: ${isLegal}` );
    console.log(`tall: ${tall}` );
    console.log(`suitable: ${suitable}` );
    console.log(`isRoyalty: ${isRoyalty}` );
    console.log(`specialCase: ${specialCase}` );
    console.log(`canEnterOurBar: ${canEnterOurBar}` );
    

}

// end page 7

// to of page 8
function fourteen()
{
    for(var i = 0; i<3; i++){
        if(i ===1)
            continue;
        
        console.log(i);
    }
}

function fiveteen()
{
    var i =0;

        while( i < 3 ){
            if( i === 1)
            {
                i = 2;
                continue;
            }
            console.log(i);
            i++;
        }
}

// end page 8

// to of page 9
function sixteen()
{

    for(var i = 0; i < 5; i++)
    {
        nexLoop2Iteration:
        for(var j=0; j < 5; j++){
            if(i==j) break nexLoop2Iteration;
            console.log(i,j);
        }
    }

}

function seventeen()
{
    function foo1()
    {
        var a = 'hello';

        function bar(){
            var b = 'world';
            console.log(a);
            console.log(b);
        }

        console.log(a);
        console.log(b);
    }

    console.log(a);
    console.log(b);
}
// end page 

// to of page 10
function foo2()
    {
        var a = 'hello';

        function bar(){
            var b = 'world';
            console.log(a);
            console.log(b);
        }

        console.log(a);
        console.log(b);
    }

// end page 10

// to of page 11

function eighteen()
{
    var a = [1,2,3,8,9,10];
    var b = [1,2,3,8,9,10];
    console.log("Antes")
    console.log("Arreglo a =",a);
    console.log("Arreglo b =",b);
    
    a.slice(0,3).concat([4,5,6,7], a.slice(3,6));
    b.slice(3,0,...[4,5,6,7]);

    console.log("Despues")
    console.log("Arreglo a =",a);
    console.log("Arreglo b =",b);

}

function nineteen()
{
    var array = ['a', 'b', 'c'];

    array.join('->');
    console.log("fecha;",array);
    array.join('.');
    console.log("punto:",array);
    'a.b.c'.split('.');
    '5.4.3.2.1'.split('.');

    
}

// end page 11


// Función onclick
function clickButton()
{
    //one();
    //two();
    //three();
    //four();
    //five();
    //six();
    //seven();
    //eight();
    //nine();
    //ten();
    //eleven();
    //twelty();
    //thirteen(); mal
    //fourteen();
    //fiveteen();
    //sixteen();
    /* seventeen();
    foo2(); */
    //eighteen();
    nineteen();

}