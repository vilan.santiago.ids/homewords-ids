let promise1 = new Promise((resolve, reject) =>{

    // Una tarea asíncrona usando setTimeout
    setTimeout( function(){
        resolve("Resolución correcta!!");
    }, 250);
});

promise1.then( mensaje=>{
    console.log(`Mensaje de resolución: ${mensaje}`);
});

// Ejemplo
let promise2 = new Promise( function(resolve, reject){
    setTimeout(function(){
        reject('DONE!');
    }, 250);
} )

.then( function(e){ console.log('done', e); } )
.catch( function(e){ console.log('done', e); } );

function divide( dividendo, divisor ){
    return new Promise((resolve, reject)=>{
        if(divisor === 0){
            reject(new Error('No se puede dividir entre 0'));
        }else{
            resolve(dividendo/divisor)
        }
    });
}

function funcion3(){
    try
    {   
        const result = divide(5, 0);
        console.log(result);
    }catch(err){
        console.error(err.mensaje)
    }
}

// Encadenamiento
function funcion4()
{
    new Promise( function(resolve, reject){
        setTimeout(function(){resolve(10); }, 250);
    })
    .then( function(num) {
        console.log('Primer then: ', num); return num * 2; })
    .then( function(num) {
        console.log('Segundo then: ', num); return num * 2; })
    .then( function(num) {
    console.log('Ultimo then: ', num); });
}

// Encadenamiento de error
function funcion5(){
    new Promise(() => {
        console.log('Inicial');
        resolver();
    })
    .then( ()=>{
        throw new Error('Algo falló');
        console.log('Haz esto');
    } )
    .catch(() => {
        console.log('Haz eso');
    })
    .then(() =>{
        console.log('Haz esto sin que importe lo que sucedió antes');
    })
}

function suma(a,b){
    return new Promise(function(resolve, reject){
        resolve( a + b );
    })
}

function restar(valor,cantidad){
    return new Promise(function(resolve, reject){
        resolve( valor - cantidad );
    })
}

function esMayorACero(valor){
    return new Promise(function(resolve, reject){
        if(valor>0){
            resolve('Mayor a cero');
        }else{
            resolve('Menor a cero');
        }
    })
}

function funcion6()
{
    const x = 10
    const y = 20    
    
    suma(x,y)
        .then( function(resultado_de_suma){
            return restar(resultado_de_suma,15);
        } )
        .then( function(resultado_de_resta){
            return esMayorACero(resultado_de_resta);
        } )
        .then( function(respuesta){
            console.log(respuesta);
        } )
}

// inconclusa
/* function funcion6( url )
{

} */

// async
function funcion7(){

}

// una promise no debe deternerte en tus procesos
// sirven para que las funciones no se queden atoradas
// son para evitar el anidamiento de los callbacks

// Función onclick
function click1()
{
    /* console.log("Promise 1:", promise1);
    console.log("Promise 2:", promise2); */
    //funcion3();
    //funcion4();
    //funcion5();
    funcion6();
}   



//console.log("Promise 2: ",promise2);
