
class SearchInput extends HTMLInputElement {

    constructor()
    {
        super();

        this.addEventListener('click', function(e){
            //alert('Input');
            console.log("Click al input text");
        });

        /* var inputM = document.createElement('input');
        this.appendChild(inputM); */

        console.log('Constructor: Input el elemento es creado');
    }


    static get observedAttributes()
    {
        return ['url'];
    }

    attributeChangedCallback(name, oldValue, newValue){

        if(name == 'url')
        {
        }
    }

}

customElements.define('mi-input',SearchInput, {extends:"input"});


class MiBotonExtendido extends HTMLButtonElement{
    constructor(){
        super();
        // escucha el link en el elemento
        this.addEventListener('click', function(e){
            console.log("evento click");
            //alert('hola gatito');
        });
    }

    static get ceName()
    {
        return 'mi-boton-extendido';
    }

    get is(){
        return this.getAttribute('is');
    }

    set is(value){
        this.setAttribute('is', value || this.ceName);
    }   
}


customElements.define('mi-boton-extendido', MiBotonExtendido, { extends:'button'} );

var myBtnSearch = document.getElementById("btn_mio");
var myBtnClear = document.getElementById("btn_mio2");

//console.log(myBtnSearch);
//console.log(myBtnClear);

myBtnSearch.addEventListener('click', filtrado );

/* myBtnSearch.addEventListener('click', function(){
    var miInput = document.getElementById("input_mio");
    var url = miInput.getAttribute("url");
    var name = miInput.value;
    console.log(url,"fdsfd", name);
    //console.log(miInput);
    //peticion( url, name );
    if( name != "" ){
        filtrado();
    }
    imprimir();
}); */


myBtnClear.addEventListener('click', limpiar );

async function peticion(url){

    var response = await fetch(url);
        if( response.ok )
        {
            /* console.log( request ); */
            const json = await response.json();
            /* console.log( typeof (json) );
            console.log( json );
            console.log( `${ json.data.length }` ); */
            tableCreate(json);
        }    
}



function tableCreate( datos )
{
    var empleados = datos.data;
    var table = document.getElementById("t1");
    for( let i = 0; i < empleados.length; i++ )
    {
        var tr = document.createElement('tr');
        table.appendChild(tr);

        var tdName = document.createElement('td');
        var tdLast = document.createElement('td');
        var tdAge = document.createElement('td');
        var tdEmp = document.createElement('td');

        console.log(empleados[i]);
        
        var names = empleados[i].employee_name.split(' ');

        tdName.appendChild(document.createTextNode(names[0]));
        tdLast.appendChild(document.createTextNode(names[1]));
        tdAge.appendChild(document.createTextNode(empleados[i].employee_age));
        tdEmp.appendChild(document.createTextNode(empleados[i].employee_salary));

        tr.appendChild(tdName);
        tr.appendChild(tdLast);
        tr.appendChild(tdAge);
        tr.appendChild(tdEmp);
    }
}

// Funcion de filtrado
function filtrado()
{
    var miInput = document.getElementById("input_mio");
    var f = miInput.value.toUpperCase();
    var table = document.getElementById("t1");
    var tr = table.getElementsByTagName("tr");
    var td, textValue;
        for( i= 1; i< tr.length; i++ )
        {
            td = tr[i].getElementsByTagName("td")[0];

                if( td )
                {
                    textValue = td.textContent || td.innerText;

                        if( textValue.toUpperCase().indexOf(f) > -1 ){
                            tr[i].style.display = "";
                        }else
                        {
                            tr[i].style.display = "none";
                        }
                }
        }
}

function limpiar()
{
    var miInput = document.getElementById("input_mio");
    var table = document.getElementById("t1").getElementsByTagName('tr');
    console.log(table);
    var iRomCount = table.length;
    console.log(iRomCount);
    miInput.value="";
        /*for( var i = 1; i < iRomCount; i++ )
        {
            table[i].style.display= "none";
        }*/
    //miInput.textContent = "";
    filtrado();
    //miInput.style.display = "none";
}


datos = peticion( 'http://dummy.restapiexample.com/api/v1/employees');