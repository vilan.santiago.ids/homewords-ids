// funciones
function imprimirTable( a )
{
    /* Agregar un titulo
    var h1 = document.createElement('h1');
    var texto = document.createTextNode('Respuesta');
    h1.appendChild(texto); */

    var table = document.getElementById('t1');
    //table.insertAdjacentElement('beforebegin',h1)
    table.border = '1';
    console.log("Tabla");
    // Creamos las celdas

    for( i of a )
    { 
        console.log(i);
        var tr = document.createElement('tr');
        table.appendChild(tr);

        var tdUser = document.createElement('td');
        var tdId = document.createElement('td');
        var tdTitle = document.createElement('td');
        var tdBody = document.createElement('td');

        tdUser.setAttribute('id','tdUser');
        tdId.setAttribute('id','tdId');
        tdTitle.setAttribute('id','tdTitle');
        tdBody.setAttribute('id','tdBody');

        tdUser.appendChild(document.createTextNode(i.userId));
        tdId.appendChild(document.createTextNode(i.id));
        tdTitle.appendChild(document.createTextNode(i.title));
        tdBody.appendChild(document.createTextNode(i.body));

        tr.appendChild(tdUser);
        tr.appendChild(tdId);
        tr.appendChild(tdTitle);
        tr.appendChild(tdBody);
    }
}

function enPantalla(text)
{
    let element = document.getElementById('container');
    //element.textContent = text;
    
    console.log(typeof(text));
    var objeto = JSON.parse(text);
    var lista1 = [];
    var lista2 = [];
    
    //console.log(objeto);

    for( x of objeto)
    {
        //console.log(x.userId+'-' + x.id );
        if( x.userId == 1 )
        {
            lista1.push(x);
        }else
            if( x.userId == 2 ){
                lista2.push(x);
            }
    }

    /* console.log(typeof(lista1) ); */
   /*  console.log(typeof(lista2)); */

    imprimirTable(lista1);
    imprimirTable(lista2);


    
}

function enconsola(text)
{
    console.log(text);
}
const req= new XMLHttpRequest();

req.addEventListener('load', () => {

    // Procesa los datos de devolución si el estatus es 200
    if( req.status === 200 ){
        // que se tiene que hacer con los datos
        //enconsola(req.responseText);

        // Formato texto
        //console.log(typeof(JSON.parse(req.responseText)));
        enPantalla(req.responseText);
        //enconsola(JSON.parse(req.responseText));
        // Formato json
        //enPantalla(req.responseText);
    }else{
        // que hacemos cuando falta
        enconsola([req.status, req.statusText]);
    }

});

req.addEventListener('error', () => {
    enconsola('Error de red');
});

// se abre una nueva solicitud ASINCRONA [true]
req.open('GET', 'https://jsonplaceholder.typicode.com/posts', true);
req.send(null);

