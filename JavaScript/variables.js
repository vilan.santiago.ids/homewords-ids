// variable global
var a = 1;

console.log("Variable a global",a);

function x(){

    console.log("Variable a global",a);
    // nivel de función
    var a = 5;

    console.log("Variable local",a);    // nivel de función
    //console.log(window.a);    // (ámbito global)
}

x();
console.log( "Variable a global", a);