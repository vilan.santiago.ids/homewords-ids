sales = [
  {
    department: 'deptoE',
    product: 'prodA',
    pieces: 5,
  },
  {
    department: 'deptoE',
    product: 'prodA',
    pieces: 5,
  },
  {
    department: 'deptoE',
    product: 'prodZ',
    pieces: 4,
  },
  {
    department: 'deptoF',
    product: 'prodZ',
    pieces: 4,
  },
  {
    department: 'deptoF',
    product: 'prodC',
    pieces: 15,
  },
  {
    department: 'deptoF',
    product: 'prodC',
    pieces: 9,
  },
];

function main() {
  var i = 0;
  while (i < sales.length) {
    var actualKey = sales[i].department;
    var department = sales[i].department;

    while (i < sales.length && sales[i].department == actualKey) {
      var actualKey2 = sales[i].product;
      var totalProdDepar = 0;
      var totalProducts = 0;
      var product = sales[i].product;

      while (
        i < sales.length &&
        sales[i].department == actualKey &&
        sales[i].product == actualKey2
      ) {
        totalProducts += sales[i].pieces;
        i++;
      }
      totalProdDepar += totalProducts;
      console.log(
        'Total ' + product + ' for ' + department + ' is ' + totalProdDepar
      );
    }
  }
}

main();
