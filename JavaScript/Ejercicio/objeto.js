
var array = [1, "pepe", 3.14, "Pepito Conejo"];

var objeto = {"nombre": "Pepito Conejo", "edad": 25, "carnet de conducir": true}


for(let i = 0; i<array.length; i++)
{
    console.log(array[i]);
}

var jsonObj = {
    "name": "Manz",
    "life": 99,
    "dead": false,
    "props": ["invisibility", "coding", "happymood"],
    "senses": {
      "vision": 50,
      "audition": 75,
      "taste": 40,
      "smell": 50,
      "touch": 80
    }
  }

  var json = {'valor1': 1, 'valor2': [1, 2, 3, 4], 'valor3': '3'};
     
// Obteniendo todas las claves del JSON
for (var clave in json){
  // Controlando que json realmente tenga esa propiedad
  if (json.hasOwnProperty(clave)) {
    // Mostrando en pantalla la clave junto a su valor
    console.log("La clave es " + clave+ " y el valor es " + json[clave]);
  }
}

console.log("JSON 1");
for( var key in jsonObj )
{
    if(jsonObj.hasOwnProperty(key)){
        console.log("El indice es "+ key + " y su valor es " + jsonObj[key] );
    }
}